const express = require('express'); 
const graphQLHTTP = require('express-graphql'); 
const mongoose = require('mongoose'); 
const schema = require('./schema/schema'); 

mongoose.connect('mongodb://localhost:27017/lyricaldb', function(){
    console.log('database connection succesfull'); 
})
const app = express(); 

app.use('/graphql', graphQLHTTP({
    schema: schema, 
    graphiql: true
}))

app.get('/api/lyrics', (req, res)=>{
    const L = require('./model/lyrical');
    L.find()
    .exec((e, l)=>{
        res.json(l);
    });
});

app.listen(8000, function(){
    console.log('server running')
})