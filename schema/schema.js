const graphql = require('graphql'); 
const LyricalModel = require('../model/lyrical'); 


const {
    GraphQLObjectType, 
    GraphQLString,
    GraphQLID,
    GraphQLInt, 
    GraphQLSchema,
    GraphQLNonNull, 
    GraphQLList
} = graphql; 



const LyricalType = new GraphQLObjectType({
    name: 'Lyrical',
    fields: ()=>({
        id: {type: GraphQLID}, 
        likes: {type: GraphQLString}, 
        content: {type: GraphQLString}
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQuery', 
    description: 'the root for the graphql lyrical', 
    fields: {
        lyricals: {
            type: new GraphQLList(LyricalType), 
            resolve(parentValue, args){
                return LyricalModel.find((err, lyrics)=>{
                   return lyrics; 
                });
            }
        }
    }
}); 


module.exports = new GraphQLSchema({
    query: RootQuery
}); 