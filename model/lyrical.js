const mongoose = require('mongoose'); 

const Schema = mongoose.Schema; 

const LyricalSchema = new Schema({
    content: String, 
    likes: String,
}); 


module.exports = mongoose.model('Lyric', LyricalSchema); 

